﻿using Newtonsoft.Json.Schema;
using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;

public class XMLValidator
{
    static string TARGET_NAMESPACE = "http://www.tcrp.gov/schema";
    static string XML = "xml";
    static string JSON = "json";

    public string validationResult { get; set; }
    public string inputString { get; set; }
    public string inputType { get; set; }
    public SelectList inputTypeOptions { get; set; }

    public string Validate(string schemaFilename, string input, string inputType)
    {
        string schemaFullPath = System.AppContext.BaseDirectory + schemaFilename;

        if (inputType == XML)
            return ValidateXML(schemaFullPath + ".xsd", input);
        else if (inputType == JSON)
            return ValidateJSON(schemaFullPath + ".json", input);
        else
        {
            this.validationResult = "Input must be in the form of either XML or JSON.";
            return this.validationResult;
        }
    }

    public string ValidateXML(string schemaFilename, string xml)
    {        
        XmlReaderSettings xmlSettings = new XmlReaderSettings();
        xmlSettings.Schemas.Add(TARGET_NAMESPACE, schemaFilename);
        xmlSettings.ValidationType = ValidationType.Schema;
        xmlSettings.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(xmlSettingsValidationEventHandler);

        XmlReader xmlReader = XmlReader.Create(GenerateStreamFromString(xml), xmlSettings);

        this.validationResult = "Passes Validation";
        this.inputString = xml;

        try
        {
            while (xmlReader.Read()) { }
        } catch (Exception e)
        {
            this.validationResult = "Exception: " + e.Message;
        }
        
        return this.validationResult;
    }

    public void xmlSettingsValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
    {
        if (e.Severity == XmlSeverityType.Warning)
        {
            this.validationResult = "Warning: " + e.Message;
        }
        else if (e.Severity == XmlSeverityType.Error)
        {
            this.validationResult = "Error: " + e.Message;
        }                     
    }

    public static Stream GenerateStreamFromString(string s)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    public string ValidateJSON (string schemaFilenameJson, string inputJson)
    {
        this.validationResult = "Passes Validation";
        this.inputString = inputJson;

        try
        {
            string json = @"" + inputJson + "";

            /*
            XNode node = JsonConvert.DeserializeXNode(json);

            String xml = node.ToString();

            int i = xml.IndexOf(">");
            xml = xml.Substring(0, i) + " xmlns=\"http://www.tcrp.gov/schema\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\"" + xml.Substring(i);

            return ValidateXML(schemaFilename, xml);
            */

            string jsonSchemaString = File.ReadAllText(schemaFilenameJson);
            JSchema schema = JSchema.Parse(jsonSchemaString);

            JToken obj = JToken.Parse(json);

            IList<string> messages;
            if (!obj.IsValid(schema, out messages))
            {
                this.validationResult = "Errors:";

                foreach (string s in messages)
                {
                    this.validationResult = this.validationResult + System.Environment.NewLine + System.Environment.NewLine + s;
                }
            }

            return this.validationResult;
        }
        catch (Exception ex)
        {
            this.validationResult = "Error converting json to xml: " + ex;
            return this.validationResult;
        }

    }

}