﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCRPXMLValidator;

namespace TCRPXMLValidator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "";

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(string inputString, string inputType, string schemaFilename = null)

        {
            if (ModelState.IsValid)
            {

                XMLValidator v = new XMLValidator();
                v.Validate(schemaFilename, inputString, inputType);

                ViewBag.inputString = v.inputString;
                ViewBag.result = v.validationResult;

                if (inputType == "xml")
                {
                    ViewBag.xmlSelected = "selected";
                    ViewBag.jsonSelected = "";
                } else
                {
                    ViewBag.xmlSelected = "";
                    ViewBag.jsonSelected = "selected";
                }

            }
            return View();
        }
    }
}
