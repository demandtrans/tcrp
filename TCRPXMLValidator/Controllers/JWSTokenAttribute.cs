﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using CnRWebServices;
using com.twjconsulting.cnr.scheduling;

public class JWSTokenAttribute : AuthorizationFilterAttribute
{

    SecurityService _securityService;

    public JWSTokenAttribute ()
    {
        _securityService = new SecurityService();
    }

    public override void OnAuthorization(HttpActionContext actionContext)
    {
        var auth = actionContext.Request.Headers.Authorization;
        PayloadToken payloadToken  = _securityService.CreatePayloadToken(auth.ToString());
        Logger.Log("DriverAppController", Logger.LogLevel.Debug, string.Format("Authorized token {0}", auth.ToString()));
        BaseAuthorizationApiController controller = actionContext.ControllerContext.Controller as BaseAuthorizationApiController;
        controller.PayloadToken = payloadToken;
    }


}
