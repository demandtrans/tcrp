﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Collections;
//using System.Web.Mvc;
using TCRPXMLValidator.Models;

namespace TCRPXMLValidator.Controllers
{
    //[Authorize]
    [RoutePrefix("api/Validator")]
    public class XMLValidatorController : ApiController
    {

        /// <summary>
        /// Validate XML against the XML Schema Definition (XSD)
        /// </summary>
        /// <param name="schema">name of the schema to be validated against</param>
        /// <param name="inputType">type of the input (xml or json)</param>
        /// <param name="input">input string (xml or json) to be validated</param>
        /// <returns>A string indicating whether or not the XML was validated successfully</returns>
        [Route("validate")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage ValidateXML(string schema, string inputType, string input)
        {
            //Logger.Log("AdminAppController", Logger.LogLevel.Debug, string.Format("Login admin {0} with password {1} ", login, password));
            if (inputType == null) inputType = "xml";
            inputType = inputType.ToLower();

            XMLValidator v = new XMLValidator();

            v.Validate(schema, input, inputType);

            return Request.CreateResponse(HttpStatusCode.OK, v.validationResult);
        }

        [Route("download")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public System.Web.Mvc.FileContentResult Download(string file)
        {
            var fileBytes = System.IO.File.ReadAllBytes(file);
            var response = new System.Web.Mvc.FileContentResult(fileBytes, "application/octet-stream")
            {
                FileDownloadName = file + ".pdf"
            };
            return response;
        }
    }
}





