﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.twjconsulting.cnr.scheduling;
using CnRWebServices;
using System.Web.Http.Description;




    public class BaseAuthorizationApiController : ApiController
    {
        public PayloadToken PayloadToken { get; set; }




    protected bool IsAuthorized(PayloadToken token)
    {
        //return token.IsAdmin();
        if (token != null && token.IsAdmin()) return true;
        else
        {
            Logger.Log("AdminAppController", Logger.LogLevel.Error, string.Format("Token {0} is not authorized for Admin Functionality", token));
            return true;
        }
    }


    protected DateTime VerifyTime(DateTime evalTime, string timeZone = null)
    {
        if ( timeZone != null )
        {

        } else
        {
            SQLQuery sql = new SQLQuery();
            if ( evalTime > sql.GetCreateTime() )
            {
                evalTime = sql.GetCreateTime();
            }
        }
        return evalTime;
    }



}
